terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
        }
    }
}

provider "vkcs" {
    username = ""
    password = ""
    project_id = "0423633f0e624bae8a82373f32be6365"
    region = "RegionOne"
}
