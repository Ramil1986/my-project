data "vkcs_compute_flavor" "HAP01" { # Выбираем имя шаблона VM (предоставляемых VK Cloud)
  name = var.compute_flavor # Имя шаблона берется из переменной 
}

data "vkcs_images_image" "HAP01" { # Выбираем имя образа VM (предоставляемых VK Cloud)
  name = var.image_flavor # Имя образа берется из переменной 
}

resource "vkcs_compute_instance" "HAP01" { # Создаем VM (HAP01)
  name                    = "HAP01" # Имя сервера (берется из переменной)
  flavor_id               = data.vkcs_compute_flavor.HAP01.id # Шаблон
  key_pair                = var.key_pair_name # Ключ SSH (настраивается в личном кабинете VK Cloud)
  security_groups         = ["default", "admin", "web"] # Группы безопасности
  availability_zone       = var.availability_zone_name # Зона доступности

  block_device {
    uuid                  = data.vkcs_images_image.HAP01.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "10.0.1.111"
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnetwork
  ]
}

resource "vkcs_networking_floatingip" "HAP01" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "HAP01" {
  floating_ip = vkcs_networking_floatingip.HAP01.address
  instance_id = vkcs_compute_instance.HAP01.id
}